import numpy as np
## this program reads in the doctopics and topicterms matrix from an lda model
# doctopics.csv and topicterms.csv --curently using dt and tt from ldavis package example
## from these two csvs create three js arrays - doctopics, topicterms and topicdocs


# input
dtfile = open("/dsl/Newspaper_Model/ParseMalletOutput/sample/sample_doc_topics.txt", 'r')

#output
ofile1 = open("/dsl/Newspaper_Model/ParseMalletOutput/dt_small.js", 'w')
#ofile2 = open("tt_small.js", 'w')
ofile3 = open("/dsl/Newspaper_Model/ParseMalletOutput/td_small.js", 'w')

#=====================================================================================
## 1. doc topics array
# top 15 topics per doc and scores
#print("computing top 15 topics per doc")
docs = []
dt = []
dnames = []
for i,line in enumerate(dtfile):

    doc = {}
    elem = line.rstrip().split(',')
    dt.append(elem[1:])
    probs = np.array(elem[1:]).astype(np.float32) #so it doesn't sort them as strings..
    inds = probs.argsort()[::-1] #decreasing
#
#
    topics = (inds[0:15] + 1).tolist() #add 1 since topics start as topic 1 not 0
#
    dnames.append(elem[0].replace('"', ''))
    doc['name'] = elem[0].replace('"', '')
    doc['scores'] = probs[inds[0:15]].tolist()
    doc['topics'] = topics
    string = str(doc.copy()) +','
    docs.append(string)
jsondocs = "var doctopics = [" + "".join(docs)[:-1] + "]"
print (jsondocs, file=ofile1)

#
##=====================================================================================
### 3. topic docs array
#
## top 15 docs per topic
print("computing topic docs array")
topic_docs = []
dnames = np.array(dnames).astype(str)
dt = np.array(dt).astype(np.float32) #19000 x 105
td = dt.transpose() #105 x 19000
for i,row in enumerate(td):
    print("topic docs on topic ", i)
    topic = {}
    inds = row.argsort()[::-1]
    tdocs = dnames[inds[0:15]].tolist() # add 1
    topic['name'] = i + 1
    topic['scores'] = row[inds[0:15]].tolist()
    topic['docs'] = tdocs
    string = str(topic.copy()) + ','
    topic_docs.append(string)

jsontopic_docs = "var topicdocs = [" + "".join(topic_docs)[:-1] + "]"
print (jsontopic_docs, file=ofile3)
#
### PRINT TO FILE
#jsondocs = "var doctopics = [" + "".join(docs)[:-1] + "]"
#jsontopic_terms = "var topicterms = [" + "".join(topic_terms)[:-1] + "]"
#print (jsondocs, file=ofile1)
#print (jsontopic_terms, file=ofile2)
#



  









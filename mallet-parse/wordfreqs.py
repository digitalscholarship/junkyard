import multiprocessing
from joblib import Parallel, delayed

ofile = open("NonSparseFreqs.dat", 'w')

## read input files
vfile = open("NonSparseTerms.dat", 'r')
cfile = open("/dsl/David_Kyle_Creativeness/24_01_2019/corpus.dat", 'r')

# get vocab from topic terms file
vocab = [word for word in vfile.readlines()]



#not concerned with memory usage here...
corpus = [doc for doc in cfile.readlines()] 

#split the corpus into chunks to be processed independently
ncores = 70
chunk_size = len(corpus) // ncores
chunks = [corpus[x:x+chunk_size] for x in range (0, len(corpus), chunk_size)]

#handle individual chunk
def getwf(chunk):
    bow = {}
    collection = " ".join(chunk)
    words = collection.split(" ")
    for w in words:
        if w not in bow:
            bow[w] = 1
        else:
            bow[w] += 1
    return bow

results = (Parallel(n_jobs=ncores)(delayed(getwf)(chunk) for chunk in chunks))

for v in vocab:
    v = v.strip()
    count = 0
    for r in results:
        if v in r:
            count = count + r[v]
    print (count, file=ofile)


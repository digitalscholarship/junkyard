import numpy as np

ifile = open("/dsl/Newspaper_Model/output/topic_counts.txt", "r")
ttfile = open("topicterms.txt", "w")
topwordsfile = open("topwords.txt", "w")

topics = [dict() for x in range(0,145)]
vocab = {}
termfromid = {}
for i, line in enumerate(open("/dsl/Newspaper_Model/Words.dat", "r")):
    vocab[line.rstrip()] = i
    termfromid[i] = line.rstrip()

for j,line in enumerate(ifile):
    if (j % 100000 == 0):
         print("on line :", j)
    elem = line.rstrip().split()
    term = elem[1]
    counts = elem[2:]
    for c in counts:
        newelem = c.split(':')
        t = int(newelem[0])
        count = newelem[1]
        topics[t][term] = count

#tt = []
for i,topic in enumerate(topics):
    print(i)
    termsarr = np.zeros(len(vocab.keys()), dtype=np.float32)
    totalwords = 0
    for k,v in topic.items():
        totalwords = totalwords + int(v)

    for k,v in topic.items():
        termsarr[vocab[k]] = float(v) / totalwords
    print(termsarr, file=ttfile)
    #tt.append(termsarr)

for j,t in enumerate(tt):
    print(j)
    inds = t.argsort()[::-1]
    toptwenty = []
    for i in range(0,20):
        word = termfromid[inds[i]]
        score = t[inds[i]]
        toptwenty.append(str(word) + ":" + str(score))
    print(" ".join(toptwenty), file=topwordsfile)

#tt = np.array(tt)
#np.savetxt("TopicTerms.dat",tt, delimiter=' ')

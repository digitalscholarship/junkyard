indfile = open("indices.txt", "r")

indices = []
for line in indfile:
    indices.append(line.rstrip())


ifile = open("/dsl/David_Kyle_Creativeness/text/info_and_content.tsv", "r")
ofile = open("/dsl/Newspaper_Model/ParseMalletOutput/corpus_sample.txt", "w")

count = 0
current = indices[count]

for i,line in enumerate(ifile,1):
    if i % 500000 == 0:
        print("on document... ", i/1000000, "million")
        print(i, current)

    if i == int(current):
        line = line.rstrip()
        elem = line.split()
        content = " ".join(elem[5:])

        print(content, file=ofile, flush=True)

        count = count + 1
        if count >= len(indices):
            break
        current = indices[count]
    


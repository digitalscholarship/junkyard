// Arthur Koehl
// single core implementation get token count for each document
// returns text file containing token count for each doc 
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strtok

int main () 
{
    setlinebuf(stdout);

    // For reading file
    char *line = NULL; 
    size_t len = 0;
    FILE *fp = fopen("/dsl/David_Kyle_Creativeness/24_01_2019/corpus.dat", "r");
    FILE *ofile = fopen("./DocTokens.dat", "w");


    // for strtok
    const char delim[] = " \n\t "; //space and tab
    char *token;
    
    // read file by line
    int docindex = 0;
    while  ((getline(&line, &len, fp)) != -1)
    {
        if (docindex % 500000 == 0)
            printf ("%f million\n", (double)docindex / 1000000);

        // split into whitespace
        token = strtok(line, delim);

        int ntokens = 0;
        
        while (token != NULL)
        {
            ntokens++; 
            token = strtok(NULL, delim);
        }//while token

        fprintf(ofile, "%d\n", ntokens);

        docindex++;
    }// while getline
    
    fclose(fp);
    fclose(ofile);

	return 0;
}




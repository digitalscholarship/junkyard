import numpy as np
# keeps only the 100,000 most frequent terms by col sum in the topic term matrix
# saves a new vocab and topic terms
NTERMS = 100000

colsums = np.loadtxt("TopicTermsColSums.dat", dtype=np.float32)
vocab = np.loadtxt("Words.dat", dtype='str')
inds = np.argsort(colsums)[::-1]
top = colsums[inds][0:NTERMS]
nsterms = vocab[inds][0:NTERMS]
np.savetxt("NonSparseTopicTerms.dat", top, delimiter=",")
np.savetxt("NonSparseTerms.dat", nsterms, fmt="%s")
np.savetxt("NonSparseIndices.dat", inds[0:NTERMS], fmt="%d")



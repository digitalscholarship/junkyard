import numpy as np
import time as time
import math
import multiprocessing
from joblib import Parallel, delayed

"""
jsPCA <- function(phi) {
  # first, we compute a pairwise distance between topic distributions
  # using a symmetric version of KL-divergence
  # http://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence
  jensenShannon <- function(x, y) {
    m <- 0.5 * (x + y)
    lhs <- ifelse(x == 0, 0, x * (log(x) - log(m)))
    rhs <- ifelse(y == 0, 0, y * (log(y) - log(m)))
    0.5 * sum(lhs) + 0.5 * sum(rhs)
  }
  dist.mat <- proxy::dist(x = phi, method = jensenShannon)
  # then, we reduce the K by K proximity matrix down to K by 2 using PCA
  pca.fit <- stats::cmdscale(dist.mat, k = 2)
  data.frame(x = pca.fit[,1], y = pca.fit[,2])
}
"""

def calc_entropy( dist ):
    entropy = 0
    for d in dist:
        if d != 0:
            entropy = entropy + d * math.log(d,2)
    return -entropy

def calc_jsd ( dists ):
    weight = float(1) / len(dists)
    A = [0] * len(dists[0]) 
    B = 0
    for dist in dists:
        for i in range (len(dist)):
            A[i] = A[i] + dist[i] * weight
        B = B + weight * calc_entropy(dist)

    return calc_entropy(A) - B

def get_distances (index):
    distances= []
    for i,d in enumerate(topicterms):
        distributions = []
        distributions.append(topicterms[index])
        distributions.append(d)
        distances.append(calc_jsd(distributions))
    print(index)
    return (index, distances)


# 1. load in the topic terms matrix
start = time.time()
print ("loading csv")
topicterms = []

ifile = open ("/dsl/Newspaper_Model/TopicTerms.dat", "r")

for i,line in enumerate(ifile):
    if i == 0:
        continue
    line = line.rstrip()
    elem = line.split(',')
    topicterms.append(np.array(elem[1:], dtype=np.float32))

print("loaded in ", time.time() - start)

start = time.time()
print ("computing distances")
results = (Parallel(n_jobs=60)(delayed(get_distances)(i) for i in range(len(topicterms))))
print ("done in ", time.time() -start)

ofile = open("results.txt", "w")
for res in results:
    print(res, file=ofile)
    


    


nonsparse = open("/dsl/David_Kyle_Creativeness/makeldavis_data/NonSparseTerms.dat", "r")
nonsparseterms = {}

for line in nonsparse:
    line = line.rstrip()
    nonsparseterms[line] = True

full = open("/dsl/David_Kyle_Creativeness/makeldavis_data/Words.dat", "r")
indfile = open("/dsl/David_Kyle_Creativeness/makeldavis_data/NonSparseIndices.dat", "w")
for i,line in enumerate(full):
    if (i %  1000000 == 0):
        print(i / 1000000)
    word = line.rstrip()
    if word in nonsparseterms:
        print(i, file=indfile)

        

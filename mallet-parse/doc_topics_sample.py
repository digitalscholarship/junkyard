import random

ifile = open("doc_topics.txt", "r")
ofile = open("sample_100000_doc_topics.txt", "w")

indices = random.sample(range(0, 8285575), 100000)
sindices = sorted(indices)

count = 0
current = sindices[count]

for i,line in enumerate(ifile):
    
    line = line.rstrip()
    elem = line.split()
    did = elem[0]
    if (i % 100000 == 0):
        print(did, current)

    if int(did) == current:
        print(did + "," + ",".join(elem[2:]), file=ofile, flush=True)
        count = count + 1
        if count >= len(sindices):
            break
        current = sindices[count]
         


import numpy as np

# load in the indices
inds = np.loadtxt("/dsl/David_Kyle_Creativeness/makeldavis_data/NonSparseIndices.dat", dtype="int")

# load in topic terms
ttfile = open("/dsl/Newspaper_Model/ParseMalletOutput/TopicTerms.dat", "r")
nonsparsett = []
for i,line in enumerate(ttfile):
    print(i)
    line = line.rstrip()
    elem = line.split()
    topic = np.asarray(elem, dtype=np.float32)
    # keep only those 100,000 columns
    nonsparsetopic = topic[inds]
    nonsparsett.append(nonsparsetopic)

nonsparsett = np.asarray(nonsparsett, dtype=np.float32)
np.savetxt("NonSparseTopicTerms.dat", nonsparsett, fmt="%f", delimiter=" ")




#!/usr/bin/python3
# Arthur Koehl
# read in doc tokens
# -n (size of sample)
# -s (min number of tokens)
import random

n = 1000 
s = 300 

# doc tokens
print("getting sample")
docs = []
tfile = open("DocTokens.dat", "r")
for i,line in enumerate(tfile): #docs start at 0
    if int(line.rstrip()) > s:
        docs.append(i)

ndocs = len(docs)

dtfile = open("/dsl/Newspaper_Model/doc_topics.txt", "r")
cfile = open("/dsl/David_Kyle_Creativeness/24_01_2019/corpus.dat", "r")
odir = "/dsl/Newspaper_Model/ParseMalletOutput/sample/"

dtofile = open(odir + "sample_doc_topics.txt", "w")

indices = random.sample(docs, n)
sindices = sorted(indices)

count = 0
current = sindices[count]

print("getting doc topics")
for i,line in enumerate(dtfile):
    
    line = line.rstrip()
    elem = line.split()
    did = elem[0]
    if (i % 100000 == 0):
        print(did, current)

    if int(did) == current:
        print(did + "," + ",".join(elem[2:]), file=dtofile, flush=True)
        count = count + 1
        if count >= len(sindices):
            break
        current = sindices[count]

## could zip the files and iterate through them both at the same time...
# but doesn't speed things up and may want to run these seperately anyways
# plus easier to debug
print ("getting sample docs")
count = 0
current = sindices[count]
for i,line in enumerate(cfile):
    if (i % 100000 == 0):
        print(i)
    if i == current:
        fname = str(i) + ".txt"
        ofile = open(odir+ fname, "w")
        print(line.rstrip(), file=ofile)
        count = count + 1
        if count >= len(sindices):
            break
        current = sindices[count]
         


import numpy as np
# load in topic terms matrix
# compute the column sums
# save to file
# send to jane
# then we will have a remove sparse.R that trims columns down

print("reading in vocab")
vocab = []
for line in open("/dsl/Newspaper_Model/ParseMalletOutput/Words.dat", "r"):
    vocab.append(line.rstrip())
vocab = np.array(vocab).astype('str')
ttfile = open("/dsl/Newspaper_Model/ParseMalletOutput/TopicTerms.dat", "r")

colSums = np.zeros(len(vocab),dtype=np.float32)
for i,line in enumerate(ttfile):
    print(i)
    elem = line.rstrip().split()
    for j,e in enumerate(elem):
        colSums[j] += float(e)

np.savetxt("TopicTermsColSums.dat", colSums)

import numpy as np


# input
vocab = []
for line in open("/dsl/Newspaper_Model/ParseMalletOutput/Words.dat", "r"):
    vocab.append(line.rstrip())
vocab = np.array(vocab).astype('str')

twfile = open("/dsl/Newspaper_Model/ParseMalletOutput/TopicTerms.dat", "r")

#output
ofile2 = open("/dsl/Newspaper_Model/ParseMalletOutput/tt_small.js", 'w')


#=====================================================================================
## 2. topic terms array
## top 300 terms per topic and scores

print("computing topic terms array")
topic_terms = []
for i,line in enumerate(twfile):
    print(i)
    topic = {}
    elem = line.rstrip().split()
    probs = np.array(elem, dtype=np.float32)
    inds = probs.argsort()[::-1]
    terms = vocab[inds[0:300]].tolist()
    name = i

    print(probs[inds][0:15])
    topic['name'] = name
    topic['scores'] = probs[inds[0:300]].tolist()
    topic['terms'] = terms
    string = str(topic.copy()) + ','
    topic_terms.append(string)

jsontopic_terms = "var topicterms = [" + "".join(topic_terms)[:-1] + "]"
print (jsontopic_terms, file=ofile2)

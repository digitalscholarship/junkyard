// Arthur Koehl
// single core implementation col sums of dtm 
// returns sum of each col  -- represents the topic frequency
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strtok

// note if ndocs is big you probably need to set ulimit to unlimited
#define NDOCS 82835575
#define NTOPICS 145

int main () 
{
    // so log statements don't get buffered
    setlinebuf(stdout);

    // For reading file
    char *line = NULL; 
    size_t len = 0;
    FILE *fp = fopen("/dsl/Newspaper_Model/doc_topics.txt", "r");
    FILE *dtfile = fopen("DocTokens.dat", "r");

    // for strtok
    const char delim[] = "\n\t "; //space and tab
    char *token;

    // initialize data
    double total = 0;
    double colsums[NTOPICS] = {0}; // one entry for each topic
    int doctokens[NDOCS]; // set ulimit unlimited in command line

    // read in doctoken data
    // use doctokens.c to calculate this
    printf("reading doc tokens data\n");
    int i = 0;
    for (i = 0; i < NDOCS; i++)
    {
        fscanf(dtfile, "%d", &doctokens[i]);
    }
    printf("done reading doc tokens\n");

    
    // read file by line
    int lc = 0; // which line you are on
    while  ((getline(&line, &len, fp)) != -1)
    {
        if (lc % 500000 == 0)
            printf ("%f million\n", (double)lc / 1000000);
        int index = 0;
        // split into whitespace
        token = strtok(line, delim);

        while (token != NULL)
        {
            if (index > 1)
            {
                colsums[index - 2] += atof(token) * doctokens[lc];
            }
               
            token = strtok(NULL, delim);
            index++;
        }//while token
        lc++;
    }// while getline
    
    i = 0;
    for (i = 0; i < NTOPICS; i++)
        printf("%f ", colsums[i]);

    fclose(fp);

    printf("done\n");
	return 0;
}



